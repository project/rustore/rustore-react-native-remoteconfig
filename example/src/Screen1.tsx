import {View, StyleSheet, TouchableOpacity, Text} from 'react-native';
import React from 'react';
import {type Screen1Prop} from './types';
import { UpdateBehaviour } from '../../src/types';

const Screen1 = ({navigation}: Screen1Prop) => {
  return (
    <View style={styles.container}>

      <TouchableOpacity onPress={() => navigation.navigate('Screen2', { updateBehaviour: UpdateBehaviour.ACTUAL})}>
        <Text>UpdateBehaviour.ACTUAL</Text>
      </TouchableOpacity>

      <TouchableOpacity onPress={() => navigation.navigate('Screen2', { updateBehaviour: UpdateBehaviour.DEFAULT})}>
        <Text>UpdateBehaviour.DEFAULT</Text>
      </TouchableOpacity>

      <TouchableOpacity onPress={() => navigation.navigate('Screen2', { updateBehaviour: UpdateBehaviour.SNAPSHOT})}>
        <Text>UpdateBehaviour.SNAPSHOT</Text>
      </TouchableOpacity>
      
    </View>
  );
};

export default Screen1;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
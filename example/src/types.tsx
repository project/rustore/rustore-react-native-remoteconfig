import type {NativeStackScreenProps} from '@react-navigation/native-stack';
import type { UpdateBehaviour } from '../../src/types';

export type StackParamList = {
  Screen1: undefined;
  Screen2: {updateBehaviour: UpdateBehaviour};
};

export type Screen1Prop = NativeStackScreenProps<StackParamList, 'Screen1'>;
export type Screen2Prop = NativeStackScreenProps<StackParamList, 'Screen2'>;
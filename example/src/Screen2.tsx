import React, { useRef, useState } from 'react';
import RemoteConfigClient, { RemoteConfigClientParams, RemoteConfigEvents, remoteConfigEventEmitter } from 'react-native-rustore-remote-config';

import { StyleSheet, View, Text, TouchableOpacity, type EmitterSubscription, TextInput, Button, Alert } from 'react-native';
import { type Screen2Prop} from './types';

 const Screen2 = ({route}: Screen2Prop) => {
  const [account, setAccountName] = useState("");
  const [language, setLanguageValue] = useState("");
  const [value, setAccountValue] = useState('');

  const listener = useRef<EmitterSubscription>();

  const checkTextInput = (value: string) => {
    if (!value.trim()) {
      return false;
    }
    return true;
  };

  const createRemoteConfig = () => {
    try {
       const behaviour = route.params.updateBehaviour

       RemoteConfigClient.createRemoteConfig(
       "ad46fa9e-8e7a-4efb-83c7-47019d327698",
       15,
       behaviour,
       new RemoteConfigClientParams({deviceModel: "SAMSUNG S24"})
      );

      listener.current = remoteConfigEventEmitter.addListener(RemoteConfigEvents.INIT_COMPLETE, () => {
      console.log("initComplete", "initComplete")
      });

      console.log("SUCCESS");
    } catch (err: any) {
      console.log(err);
    }

    return () => {
      listener.current?.remove();
    };
  };

  const getRemoteConfig = async () => {
    try {
        let config = await RemoteConfigClient.getRemoteConfig();
        console.log(JSON.stringify(config));
      //  setAccountValue(await RemoteConfigClient.getString("SDK_auff_sample"));

        // type data = {
        //     data : {
        //       age?: string,
        //       example?: string,
        //       background?: string,
        //       hero_color: string
        //     }
        // };

        // let obj: data = JSON.parse(config);
        // console.log(obj.data.age);
        // console.log(obj.data.example);
        // console.log(obj.data.background);
        // console.log(obj.data.hero_color);

    } catch (err: any) {
        console.log(err);
    }
  };

  const init = async() => {
    try {
      await RemoteConfigClient.init();
    } catch (err: any) {
      console.log(err);
    }
  };

  const setAccount = (account: string) => {
    try {
      RemoteConfigClient.setAccount(account);
    } catch (err: any) {
      console.log(err);
    }
  };

  const setLanguage = (language: string) => {
    try {
        RemoteConfigClient.setLanguage(language);
    } catch (err: any) {
      console.log(err);
    }
  };

  return (
    <View style={styles.container}>

      <TextInput placeholder="Enter account"
        value={account} style={styles.textInput} onChangeText={(value) => setAccountName(value)} />
       <Button title="Set account" onPress={ () => {
        if(checkTextInput(account)) {
            setAccount(account);
            setAccountName("")
        } else {
            Alert.alert("Предупреждение", "Поле не может быть пустым")
        }
    }}/>

      <TextInput placeholder="Enter language"
     value={language} style={styles.textInput}onChangeText={(value) => setLanguageValue(value)} />
       <Button title="Set language" onPress={ () => {
         if(checkTextInput(language)) {
             setLanguage(language);
             setLanguageValue("")
         } else {
             Alert.alert("Предупреждение", "Поле не может быть пустым")
         }
     }}/>

      <TouchableOpacity onPress={createRemoteConfig}>
          <Text>createRemoteConfig</Text>
      </TouchableOpacity>

      <TouchableOpacity onPress={init}>
         <Text>init</Text>
      </TouchableOpacity>

      <TouchableOpacity onPress={getRemoteConfig}>
          <Text> getRemoteConfig</Text>
      </TouchableOpacity>
      <Text>{value}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  textInput: {
    width: "80%",
    borderRadius: 20,
    paddingVertical: 8,
    paddingHorizontal: 16,
    marginTop: 20,
    borderColor: "rgba(0, 0, 0, 0.2)",
    borderWidth: 1,
    marginBottom: 8,

},
});

export default Screen2;

package com.remoteconfig.utils

enum class ReactUpdateBehaviour {
  ACTUAL,
  DEFAULT,
  SNAPSHOT
}

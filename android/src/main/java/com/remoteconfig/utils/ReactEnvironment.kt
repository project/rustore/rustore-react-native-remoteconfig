package com.remoteconfig.utils

enum class ReactEnvironment {
  ALPHA,
  BETA,
  RELEASE
}

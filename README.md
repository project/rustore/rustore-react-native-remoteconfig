## RuStore React Native Remote Config SDK для работы с облачным сервисом конфигурации приложения

### [🔗 Документация разработчика](https://www.rustore.ru/help/developers/tools/remote-config)

### Подключение в проект

```sh
// HTTPS
npm install git+https://git@gitflic.ru/project/rustore/rustore-react-native-remoteconfig.git

// SSH
npm install git+ssh://git@gitflic.ru/project/rustore/rustore-react-native-remoteconfig.git
```

### Сборка примера приложения

Вы можете ознакомиться с демонстрационным приложением содержащим представление работы всех методов sdk:
- [README](example/README.md)
- [example](https://gitflic.ru/project/rustore/rustore-react-native-remoteconfig/file/?file=example&branch=master)


### Условия распространения
Данное программное обеспечение, включая исходные коды, бинарные библиотеки и другие файлы распространяется под лицензией MIT. Информация о лицензировании доступна в документе [MIT-LICENSE](MIT-LICENSE.txt).


### Техническая поддержка
Дополнительная помощь и инструкции доступны на странице [help.rustore.ru](https://help.rustore.ru/).
